import { Prerequisites } from '@rocketmakers/shell-commands/lib/prerequisites';
import { Args } from '@rocketmakers/shell-commands/lib/args';
import { Shell } from '@rocketmakers/shell-commands/lib/shell';
import { setDefaultLoggerLevel, createLogger } from '@rocketmakers/shell-commands/lib/logger';

const logger = createLogger('deploy');

Prerequisites.register({
  command: 'nodenv',
  description: 'Node Env',
  installInstructions: 'brew install nodenv',
});

async function run() {
  const args = await Args.match({
    log: Args.single({
      description: 'The log level',
      shortName: 'l',
      defaultValue: 'info',
      validValues: ['trace', 'debug', 'info', 'warn', 'error', 'fatal'],
    }),
    environment: Args.single({
      description: 'Build Environment',
      shortName: 'e',
      defaultValue: 'dev',
      validValues: ['dev', 'staging', 'production'],
    }),
  })

  if (!args) {
    throw new Error("No Arguments Set");
  }

  try {
    setDefaultLoggerLevel(args.log as any);

    const nodeVersion = await Shell.exec('cat', ['../.node-version']);
    await Shell.exec('nodenv', ['update-version-defs']);
    await Shell.exec('yes', ['n', '|', 'nodenv', 'install', nodeVersion]);

    // Build Cordova
    await Shell.exec('bundle', ['install', '--path', 'vendor/bundle', '--full-index']);
    await Shell.exec('bundle', ['exec', 'fastlane', 'android', `deploy${args.environment}`]);
    await Shell.exec('bundle', ['exec', 'fastlane', 'ios', `deploy${args.environment}`]);
  } catch (error) {
    logger.error(error);
    process.exit(-1);
  }
}

run().catch(error => {
  logger.error(error);
  process.exit(-1);
});