## 100things

### Requirements

- Node 12.13.0
- Android Studio + latest Android SDKS
- XCode 11.x

### How to use

In root directory run

```
npm i 
npm start
```

### Firebase

Not Implemented yet

### Analytics

Not Implemented yet