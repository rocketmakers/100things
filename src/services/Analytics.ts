import * as React from 'react';

import { AnalyticsEvent } from './analytics/events';
import { ScreenName } from './analytics/screens';

export namespace Analytics {
  const parseStringName = (input: string) =>
    input === null ? null : input.replace(/(-|\s)/g, '_');

  const execute = (
    method: () => any,
    log?: { title: string; body: any[] | any },
  ) => {
    // tslint:disable: no-string-literal no-extra-boolean-cast
    if (!!window['FirebasePlugin']) {
      method();
    } else if (APP_CONFIG.environment === 'test' && log) {
      // tslint:disable: no-console
      console.log(
        `${log.title.toUpperCase()}:\n`,
        ...(Array.isArray(log.body) ? log.body : [log.body]),
      );
    }
  };

  export const logEvent = (name: AnalyticsEvent, params: {}) => {
    execute(() =>
      // tslint:disable: no-string-literal
      window['FirebasePlugin'].logEvent(parseStringName(name), params),
    );
  };

  export const setUserId = (id: string) => {
    // tslint:disable: no-string-literal
    execute(() => window['FirebasePlugin'].setUserId(id));
    // tslint:disable: no-string-literal
    execute(() => window['FirebasePlugin'].setCrashlyticsUserId(id));
  };

  export const clearUserId = () => {
    setUserId('anonymous');
  };

  export const setUserProperty = (name: string, value: string) => {
    // tslint:disable: no-string-literal
    execute(
      () =>
        window['FirebasePlugin'].setUserProperty(
          parseStringName(name),
          parseStringName(value),
        ),
      {
        body: [`name: ${name}`, `value: ${value}`],
        title: 'Set User Property',
      },
    );
  };

  export const clearUserProperty = (name: string) => {
    setUserProperty(parseStringName(name), '');
  };

  export const setScreenName = (name: ScreenName) => {
    // tslint:disable: no-string-literal
    execute(
      () => window['FirebasePlugin'].setScreenName(parseStringName(name)),
      {
        body: parseStringName(name),
        title: 'Set screen name',
      },
    );
  };

  export const clearCurrentScreen = () => {
    setScreenName(null);
  };

  export const setAnalyticsCollectionEnabled = (value: boolean) => {
    // tslint:disable: no-string-literal
    execute(
      () => window['FirebasePlugin'].setAnalyticsCollectionEnabled(value),
      {
        body: value,
        title: 'set analytics enabled',
      },
    );
  };

  export const enable = () => {
    setAnalyticsCollectionEnabled(true);
  };

  export const disable = () => {
    setAnalyticsCollectionEnabled(false);
  };
}
