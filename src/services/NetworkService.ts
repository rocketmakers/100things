export type NetworkStatus = "offline" | "cell" | "cell-2g" | "cell-3g" | "cell-4g" | "ethernet" | "none" | "unknown" | "wifi"

class NetworkService {

  public status(): NetworkStatus {
    switch (navigator.connection.type) {
      case Connection.CELL:
        return "cell";
      case Connection.NONE:
        return "offline";
      case Connection.CELL_2G:
        return "cell-2g";
      case Connection.CELL_3G:
        return "cell-3g";
      case Connection.CELL_4G:
        return "cell-4g";
      case Connection.ETHERNET:
        return "ethernet";
      case Connection.WIFI:
        return "wifi";
      default:
        return "unknown";
    }
  }

  public isStrong() {
    const status = this.status();
    return status === "wifi" || status === "ethernet"
  }

  public isMedium() {
    const status = this.status();
    return status === "cell-4g"
  }

  public isWeak() {
    return !this.isStrong() && !this.isMedium() && !this.isOffline()
  }

  public isOffline() {
    const status = this.status();
    return status === "offline";
  }
}

export const networkService = new NetworkService();
