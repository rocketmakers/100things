class ActionSheetService {
  public showActionSheet(labels: string[], callback: (value: string) => void) {
    const options = {
      // tslint:disable: no-string-literal
      addCancelButtonWithLabel: 'Cancel',
      androidEnableCancelButton: true, // default false
      androidTheme:
        window['plugins'].actionsheet.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
      buttonLabels: labels,
      title: 'Select an option',
    };
    // tslint:disable-next-line: no-string-literal
    window['plugins'].actionsheet.show(options, cb => {
      const index = cb - 1;
      callback(labels[index]);
    });
  }
}

export const actionSheetService = new ActionSheetService();
