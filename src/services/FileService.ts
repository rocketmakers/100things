class FileService {
  private systemDir = () => cordova.file.cacheDirectory;

  private getSystemDir = async () =>
    new Promise<DirectoryEntry>(res =>
      window.resolveLocalFileSystemURL(
        this.systemDir(),
        e => res(e as DirectoryEntry),
        // tslint:disable-next-line:no-console
        e => console.error(e),
      ),
    );

  private base64toBlob(data: string, contentType: string) {
    const sliceSize = 512;
    const byteCharacters = atob(data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  private async getFile(fileName: string, dirName?: string) {
    const dir = await (dirName ? this.getDir(dirName) : this.getSystemDir());
    return new Promise<FileEntry>(res =>
      dir.getFile(fileName, { create: true }, res),
    );
  }

  public async readFileAsText(
    filename: string,
    dirName?: string,
  ): Promise<string> {
    const fileEntry = await this.getFile(filename, dirName);
    return new Promise<string>((res, rej) => {
      fileEntry.file(file => {
        const reader = new FileReader();
        reader.onloadend = function() {
          res(this.result as string);
        };
        reader.readAsText(file);
      }, rej);
    });
  }

  public async base64StringToFile(
    base64: string,
    filename: string,
    dirName?: string,
    type: string = 'text/plain',
  ) {
    return this.blobToFile(this.base64toBlob(base64, type), filename, dirName);
  }

  public async stringToFile(
    str: string,
    filename: string,
    dirName?: string,
    type: string = 'text/plain',
  ) {
    return this.blobToFile(new Blob([str], { type }), filename, dirName);
  }

  public async blobToFile(blob: Blob, filename: string, dirName?: string) {
    const file = await this.getFile(filename, dirName);
    return new Promise<string>((res, rej) => {
      file.createWriter(fw => {
        fw.onwriteend = () => {
          const finalPath =
            this.systemDir() + (dirName ? `${dirName}/` : '') + filename;
          res(finalPath);
        };
        fw.onerror = rej;
        fw.write(blob);
      });
    });
  }

  public async getDir(dirName: string) {
    const dir = await this.getSystemDir();
    return new Promise<DirectoryEntry>((res, rej) => {
      dir.getDirectory(dirName, { create: true }, res, rej);
    });
  }

  public async removeDir(dirName: string) {
    const dir = await this.getDir(dirName);
    return new Promise<void>((res, rej) => {
      dir.removeRecursively(res, rej);
    });
  }

  public async removeFile(filename: string, dirName?: string) {
    const file = await this.getFile(filename, dirName);
    return new Promise<void>((res, rej) => {
      file.remove(res, rej);
    });
  }
}

export const fileService = new FileService();
