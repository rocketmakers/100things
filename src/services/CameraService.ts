export enum SourceType {
  Camera,
  PhotoLibrary,
}

class CameraService {
  public getCordovaSourceType(sourceType: SourceType) {
    switch (sourceType) {
      case SourceType.Camera: {
        return Camera.PictureSourceType.CAMERA;
      }
      case SourceType.PhotoLibrary: {
        return Camera.PictureSourceType.SAVEDPHOTOALBUM;
      }
      default: {
        return Camera.PictureSourceType.CAMERA;
      }
    }
  }

  public getPhoto(
    success: (image: string) => void,
    error?: (errorMessage: string) => void,
    sourceType?: SourceType,
    saveToLibrary = true,
  ) {
    if (window.cordova === undefined) {
      success('');
    } else {
      const cordovaSourceType = sourceType
        ? this.getCordovaSourceType(sourceType)
        : Camera.PictureSourceType.CAMERA;

      const config: CameraOptions = {
        allowEdit: false,
        cameraDirection: Camera.Direction.BACK,
        correctOrientation: true,
        destinationType: Camera.DestinationType.FILE_URI,
        encodingType: Camera.EncodingType.JPEG,
        mediaType: Camera.MediaType.PICTURE,
        saveToPhotoAlbum: saveToLibrary,
        sourceType: cordovaSourceType,
      };
      navigator.camera.getPicture(
        url => {
          success(url);
        },
        errorMessage => {
          error(errorMessage);
        },
        config,
      );
    }
  }
}

export const cameraService = new CameraService();
